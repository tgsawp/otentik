<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


	<title>Inventarisasi Aset</title>
	<link rel="stylesheet" href="assets/style.css">
	<link rel="stylesheet" href="assets/bootstrap-4.3.1-dist/css/bootstrap.min.css">
	<script src="assets/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
	<script src="assets/jquery-3.4.1.min.js"></script>
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light navbar-laravel">
			<div class="container">
				<a class="navbar-brand" href="#">Inventarisasi Aset</a>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
					<?php
					if(Session::exists('username')){
					?>
						<li class="nav-item">
							<a class="nav-link" href="logout.php">Logout</a>
						</li>
					<?php } else{ ?>
						<li class="nav-item">
							<a class="nav-link" href="login.php">Sign In</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="register.php">Register</a>
						</li>
					<?php } ?>
						<li class="nav-item">
							<a class="nav-link" href="profil.php">Profile</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
