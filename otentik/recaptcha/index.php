<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>TUTORIALWEB.NET | reCAPTCHA</title>
 
	<!-- Link untuk Bootstrap, jadi harus online ya :) -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<style>
		.g-recaptcha {
			margin-bottom: 10px;
		}
	</style>
</head>
<body>
 
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h3>Daftar</h3>
				<hr>
				<form action="daftar.php" method="post" autocomplete="off">
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" name="email" class="form-control" id="email" placeholder="anda@domain.com" required>
					</div>
					
					<!-- Div di bawah ini untuk menampilkan reCAPTCHA nya. Pada gambar diatas masukkan yang ada di kotak biru -->
					<div class="g-recaptcha" data-sitekey="6Lf7WCUTAAAAANJ2ZMQ1Xvum9wXMo_6tGcl_rYej"></div>
					
					<input type="submit" name="submit" value="Daftar" class="btn btn-primary">
				</form>
			</div>
		</div>
	</div>
	
	<!-- Link javascript untuk api reCAPTCHA, pada gambar di atas masukkan pada kotak warna merah -->
	<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>