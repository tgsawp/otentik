<?php
	require_once "config/init.php";
	/*if(Session::exists('username')){
		//header('Location: profil.php');
		Redirect::to('profil.php');
	}*/

	/* Nama = Kayan Adonis Wihardy
	   NIM = 2018104233*/
	   
	if($pegawai->isLoggedIn()){
		Redirect::to('profil');
	}
	//validasi
	if(Input::get('submit')){
		if(Token::check(Input::get('token'))){
		//call validation object
		$validasi = new Validasi();

		//metode check
		$validasi = $validasi->check(array(
			'username' => array('required' =>true),
			'password' => array('required' => true)
			));
		//lolos validasi
		if($validasi->passed()){
			if($pegawai->cekUserName(Input::get('username'))){
				if($pegawai->login_pegawai(Input::get('username'), Input::get('password')) ){
					Session::set('username', Input::get('username'));
					//header('location: profil.php');
					Redirect::to('profil');
				}else{
					//echo 'Login gagal';
					$errors [] = 'Login gagal';
				}
			}else{
				$errors [] = 'Username ini tidak ada';
			}
		}else{
			//print_r($validasi->errors());
			$errors = $validasi->errors();
		}
	}
	}

	require_once 'templates/header.php';
?>

<?php if(!empty($errors)){ ?>
	<div id="errors">
	<?php foreach($errors as $error){ ?>
		<li> <?php echo $error; ?></li>
	<?php } ?>
	</div>
<?php } ?>

<main class ="login-form">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">Log In</div>
						<div class="card-body">
						<img src="assets/icon.jpg" alt="Avatar" class="avatar">
							<form action="login.php" method="post">
								<div class="form-group row">
									<label for="username" class="col-md-4 col-form-label text-md-right">Username :</label>
									<div class="col-md-6">
										<input type="text" id="email_address" class="form-control" name="username" required autofocus>
									</div>
								</div>

								<div class="form-group row">
									<label for="password" class="col-md-4 col-form-label text-md-right">Password :</label>
									<div class="col-md-6">
										<input type="password" id="password" class="form-control" name="password" required>
									</div>
								</div>

								<div class="col-md-6 offset-md-5">
									<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
									<input type="submit" class="btn btn-success" name="submit" value="Log In">
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
</main>
<?php
	require_once 'templates/footer.php';
?>