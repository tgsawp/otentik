<?php
	require_once 'config/init.php';
	if(!$pegawai->isLoggedIn()){
		Session::flash('login', 'Anda harus login terlebih dahulu');
		Redirect::to('login');
	}
	if(!$pegawai->isAdmin(Session::get('username'))){
		Session::flash('profil', 'Halaman khusus Admin');
		Reidrect::to('profil');
	}
	$tampilPegawai= $pegawai->getPegawai();
	require_once 'templates/header.php';
?>



<main class ="admin-form">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6">
						<div class="card">
							<div class="card-header">Halaman Admin</div>
							<div class="card-body">
								<div class="row">
									<div class="col-md-6">
										<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist">
												<?php foreach($tampilPegawai as $_peg) : ?>
												<div class="">
													<a href="profil.php?nama=<?php echo$_peg['kode'] ?>">
														<?php echo $_peg['kode'] ?> </a>
														<?php echo $_peg['nama'] ?> </a>
												</div>
											<?php endforeach; ?>
										</div>
									</div>
								</div>
						</div>
				</div>
		</div>
</main>



<?php require_once 'templates/footer.php'; ?>


