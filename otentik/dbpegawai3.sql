-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2019 at 02:27 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbpegawai3`
--

-- --------------------------------------------------------

--
-- Table structure for table `taset1`
--

CREATE TABLE `taset1` (
  `id` int(11) NOT NULL,
  `kodeB` varchar(10) NOT NULL,
  `namaB` varchar(30) NOT NULL,
  `jenisB` varchar(10) NOT NULL,
  `jumlah` int(255) NOT NULL,
  `lokasi` varchar(30) NOT NULL,
  `kondisi` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tpegawai1`
--

CREATE TABLE `tpegawai1` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tpegawai1`
--

INSERT INTO `tpegawai1` (`id`, `kode`, `password`, `nama`) VALUES
(1, 'brino', '$2y$10$dk8Q0ykwnav2h37nuHoytuTDm.33L5KtkVxCENPs9l2LE7c2UIBrq', 'Brino Jefferson'),
(5, 'brownie', '$2y$10$Sg1Hb7EdOqyPUcSK3oQFbuIUhAch4PkEC9.HOFSEDCRHHt6SkAIDK', 'Ester Lumba');

-- --------------------------------------------------------

--
-- Table structure for table `tpegawai2`
--

CREATE TABLE `tpegawai2` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '0',
  `Kelamin` varchar(9) NOT NULL,
  `image` blob NOT NULL,
  `tLahir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tpegawai2`
--

INSERT INTO `tpegawai2` (`id`, `kode`, `password`, `nama`, `role`, `Kelamin`, `image`, `tLahir`) VALUES
(14, 'brino', '$2y$10$D7s0JieEeutRLMYWsw7gMuM5K57LjI6hMaX/HBmXOlufbfcP.nJRi', 'Brino Jefferson', 1, 'Laki-laki', 0x505f32303136303831325f3135343732345f42465f322e6a7067, '2019-05-14'),
(16, 'blessy', '$2y$10$qjvPSLmrPB1r8Z62l67KBeoN3S3Rl4R6BsdqANMbG4O.V0pqL1.7K', 'Blessy Jeniffer', 0, 'Perempuan', 0x313535383537373034313830382e6a7067, '2005-05-03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `taset1`
--
ALTER TABLE `taset1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tpegawai1`
--
ALTER TABLE `tpegawai1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tpegawai2`
--
ALTER TABLE `tpegawai2`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `taset1`
--
ALTER TABLE `taset1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tpegawai1`
--
ALTER TABLE `tpegawai1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tpegawai2`
--
ALTER TABLE `tpegawai2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
