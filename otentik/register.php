<?php
	require_once 'config/init.php';
	/*if(Session::exists('username')){
		//header('Location: profil.php');
		Redirect::to('profil');
	}*/
	if($pegawai->isLoggedIn()){
		Redirect::to('profil');
	}
	//validasi
	if(Input::get('submit')){
		$target_dir  = "assets/img/";
		$target_file = $target_dir . basename($_FILES["photo"]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

			if(move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)){
			}else{
				echo "Sorry, there was an error uploading your file.";
			}
			$upload = basename($_FILES["photo"]["name"]);
		//call validation object
		$validasi = new Validasi();

		//metode check
		$validasi = $validasi->check(array(
			'username' => array(
						'required' => true,
						'min' => 4,
						'max' => 10,
					),
			'password' => array(
						'required' => true,
						'min' => 4,
					),
			'txtNama' => array(
						'required' => true,
						'min' => 4,
						'max' => 30,
					),
			'password_verify' => array(
						'required' => true,
						'match' => 'password'
					),
			'jenis' => array(
						'required' => true,
					),
			'lahir' => array(
						'required' => true,
			)
		));
		if($pegawai->cekUserName(Input::get('username'))){
			$errors[] = "Nama sudah terdaftar";
		}else{
		//lolos validasi
		if($validasi->passed()){
	

			$pegawai->register_pegawai(array(
				//key =>value
				'kode' => Input::get('username'),
				'password' => password_hash(Input::get('password'), PASSWORD_DEFAULT),
				'nama' => Input::get('txtNama'),
				'kelamin' => Input::get('jenis'),
				'image' => $upload,
				'tLahir' => Input::get('lahir')
			));

			Session::flash('profil','Selamat pendaftaran berhasil');
			Session::set('username', Input::get('username'));
			Session::set('jenis', Input::get('jenis'));
			//header('location: profil.php');
			Redirect::to('profil');


		}else{
			//print_r($validasi->errors());
			$errors = $validasi->errors();
		}
	}
}
	require_once 'templates/header.php';
?>
<?php if(!empty($errors)){ ?>
	<div id="errors">
	<?php foreach($errors as $error){ ?>
		<li> <?php echo $error; ?> </li>
	<?php } ?>
	</div>
<?php } ?>
<!--
<form action="register.php" method="post" enctype="multipart/form-data">

		<label>Username	:</label><input type="text" name="username"><br>
		<label>Password	:</label><input type="password" name="password"><br>
		<label>Retype Password  :</label><input type="password" name="password_verify"><br>
		<label>Nama		:</label><input type="text" name="txtNama"><br>
		<label>Jenis Kelamin :</label><input type="radio" name="jenis" value="Laki-laki">Laki-laki
		<input type="radio" name="jenis" value="perempuan">Perempuan<br>
   		<label>Foto 	: </label><input type="file" name="photo" id="photo" accept="image/*" onclick="loadfile.click()" /><br>
   		<img id="output"/>
   		<script>
   			var loadFile = function(event){
   				var output = document.getElementById('output');
   				output.src = URL.createObjectURL(event.target.files[0]);
   			};
   		</script>
		<input type="submit" name="submit" value="  Register  ">

</form>
		-->

		<main class ="register-form">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">Register</div>
						<div class="card-body">
							<form action="register.php" method="post" enctype="multipart/form-data">
								<div class="form-group row">
									<label for="username" class="col-md-4 col-form-label text-md-right">Username :</label>
									<div class="col-md-6">
										<input type="text" class="form-control form-control-sm" name="username" required autofocus>
									</div>
								</div>

								<div class="form-group row">
									<label for="password" class="col-md-4 col-form-label text-md-right">Password :</label>
									<div class="col-md-6">
										<input type="password" class="form-control form-control-sm" name="password" required>
									</div>
								</div>

								<div class="form-group row">
									<label for="password" class="col-md-4 col-form-label text-md-right">Retype Password :</label>
									<div class="col-md-6">
										<input type="password" class="form-control form-control-sm" name="password_verify" required>
									</div>
								</div>

								<div class="form-group row">
									<label for="nama" class="col-md-4 col-form-label text-md-right">Nama :</label>
									<div class="col-md-6">
										<input type="text" class="form-control form-control-sm" name="txtNama" required>
									</div>
								</div>

								<div class="form-group row">
									<label for="jenis" class="col-md-4 col-form-label text-md-right">Jenis Kelamin :</label>
									<div class="col-md-6">
										<input type="radio"  class="radio-inline" name="jenis" value="Laki-laki"> Laki-laki   
										<input type="radio"  class="radio-inline" name="jenis" value="Perempuan"> Perempuan 
									</div>
								</div>

								<div class="form-group row">
									<label for="lahir" class="col-md-4 col-form-label text-md-right">Tanggal Lahir :</label>
									<div class="col-md-6">
										<input type="date" class="form-control form-control-sm" name="lahir" required>
									</div>
								</div>

								<div class="form-group row">
									<label for="foto" class="col-md-4 col-form-label text-md-right">Foto :</label>
									<div class="col-md-6">
									<input type="file" name="photo" id="photo" accept="image/*" onchange="loadFile(event)" />
									<img id="output"/>
									<script>
										var loadFile = function(event){
										var output = document.getElementById('output');
										output.src = URL.createObjectURL(event.target.files[0]);
   										};
   									</script>
									</div>
								</div>

								<div class="col-md-6 offset-md-5">
									<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
									<input type="submit" class="btn btn-success" name="submit" value="Register">
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
</main>
<?php
	require_once 'templates/footer.php';
?>