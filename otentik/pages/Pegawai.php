<?php
	class Pegawai{
		private $_db;
		public function __construct(){
			$this->_db = Database::getInstance();
		}
		public function register_pegawai($fields = array()){
			if($this->_db->insert('tpegawai2', $fields))
				return true;
			else
				return false;
		}

		public function login_pegawai($username, $password){
			$data = $this->_db->getInfo('tpegawai2', 'kode', $username);
			//print_r($data);
			//die();
			if(password_verify($password, $data['password'])){
				return true;
			}else{
				return false;
			}
		}
		public function cekUserName($username){
			$data = $this->_db->getInfo('tpegawai2', 'kode', $username);
			if(empty($data)){
				return false;
			}else{
				return true;
			}
		}

		public function isAdmin($username){
			$data = $this->_db->getInfo('tpegawai2', 'kode', $username);
			if($data['role'] == 1){
				return true;
			}else{
				return false;
			}
		}

		public function isLoggedIn(){
			if(Session::exists('username'))
				return true;
			else
				return false;
		}

		public function getData($username){
			if($this->cekUserName($username)){
				return $this->_db->getInfo('tpegawai2', 'kode', $username);
			}else{
				return die("Nama ini tidak terdaftar");
			}
		}

		public function updatePegawai($fields = array(), $id){
			if($this->_db->update('tpegawai2', $fields, $id))
				return true;
			else
				return false;
		}

		public function getPegawai(){
			return $this->_db->getInfo('tpegawai2');
		}

		public function delPegawai($id){
			if($this->_db->delete($id))
				return true;
			else
				return false;
		}
	}
?>