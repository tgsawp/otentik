<?php
	class Database{
		private static $INSTANCE = null;
		private $con,
		$HOST = 'localhost',
		$USER = 'root',
		$PASS = '',
		$DBNAME = 'dbpegawai3';

		public function __construct(){
			$this->con = new mysqli($this->HOST, $this->USER, $this->PASS, $this->DBNAME);
			if(mysqli_connect_error()){
				die('Tidak terhubung ke database');
			}
		}
		/*singleton pattern, menguji koneksi agar tidak double */
		public static function getInstance(){
			if(!isset(self::$INSTANCE)){
				self::$INSTANCE = new Database();
			}
			return self::$INSTANCE;
		}

		public function insert($table, $fields = array()){
			//mengambil column
			$column = implode(", ", array_keys($fields));

			//mengambil nilai
			$valueArrays = array();
			$i = 0;
			foreach($fields as $key=>$values){
				if(is_int($values)){
					$valueArrays[$i] = $this->escapeInput($values);
				}else{
					$valueArrays[$i] = "'".$this->escapeInput($values)."'";
				}
				$i++;
			}
			$values = implode(", ", $valueArrays);
			$query = "INSERT INTO $table($column) Values($values)";

			return $this->run_query($query, 'Insert data gagal');
		}

		public function run_query($query, $msg){
			if($this->con->query($query))
				return true;
			else
				die ($msg);
		}

		public function escapeInput($name){
			return $this->con->real_escape_string($name);
		}

		public function getInfo($table, $column='', $value=''){
			if(!is_int($value)){
				$value = "'".$value."'";
			}
			if($column != ''){
			$query = "SELECT * FROM $table WHERE $column = $value";
			$result = $this->con->query($query);

			while($row = $result->fetch_assoc()){
				return $row;
			}
		}else{
			$query = "SELECT * FROM $table";
			$result = $this->con->query($query);
			while($row = $result->fetch_assoc()){
				$resuts[] = $row;
			}
			return $resuts;
		}
	}

		public function update ($table, $fields, $id){
			//mengambil nilai
			$valueArrays = array();
			$i = 0;
			foreach($fields as $key=>$values){
				//UPDATE TABLE SET kunci1 = kunci2 = nilai ....
				if(is_int($values)){
					$valueArrays[$i] = $key ."=". $this->escapeInput($values);
				}else{
					$valueArrays[$i] = $key ."='". $this->escapeInput($values)."'";
				}
				$i++;
			}
			$values = implode(", ", $valueArrays);
			$query = "UPDATE $table SET $values WHERE id = $id";
			//die($query);
			return $this->run_query($query, 'Ada masalah saat update');
		}

		public function delete($id){
			$query = "DELETE FROM tpegawai2 WHERE id = $id ";
			return $this->run_query($query, 'Ada masalah saat delete');
		}
	}
	//Database::getInstance();
	//$db = Database::getInstance();
	//var_dump($db);
?>