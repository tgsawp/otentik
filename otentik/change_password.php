<?php
	require_once 'config/init.php';
	if(!$pegawai->isLoggedIn()){
		Session::flash('login', 'Anda harus login');
		Redirect::to('login');
	}

	$dataPegawai = $pegawai->getData(Session::get('username'));
	$errors = array();
	if(Input::get('submit')){
		if(Token::check(Input::get('token'))){
			$validasi = new Validasi();
			//metode check
			$validasi = $validasi->check(array(
				'password' => array('required' => true),
				'newPassword' => array('required' => true,
										'min' => 4,
										),
				'verifyPassword' => array('required' => true,
									'match' => 'newPassword'
									)
					));

			//lolos uji
			if($validasi->passed()){
				if(password_verify(Input::get('password'), $dataPegawai['password'])){
					$pegawai->updatePegawai(array(
						'password' => password_hash(Input::get('newPassword'), PASSWORD_DEFAULT)//,
						//'nama' => 'Brino Ganteng'
					), $dataPegawai['id']);

					Session::flash('profil', 'selamat! anda berhasil mengganti password');
					Redirect::to('profil');
				}else{
					$errors[] = 'password lama salah';
				}	
			}else{
				$errors = $validasi->errors();
			}
		}
	}
	require_once 'templates/header.php';
?>

<?php if(!empty($errors)){ ?>
	<div id="errors">
	<?php foreach($errors as $error){ ?>
		<li> </?php echo $error; ?> </li>
	<?php } ?>
	</div>
	<?php } ?>
<!--
<form action="change_password.php" method="post">
	<label>Old Password:</label><input type="password" name="password"><br>
	<label>New Password:</label><input type="password" name="newPassword"><br>
	<label>New Password Verify</label><input type="password" name="verifyPassword"><br>
	<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
	<input type="submit" name="submit" value=" Change Password ">
</form>
-->




<main class ="change-form">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">Change Password<br><h4>Halo! 
<?php
	echo $dataPegawai['nama'];
?></h4></div>
						<div class="card-body">
							<form action="change_password.php" method="post" enctype="multipart/form-data">
								<div class="form-group row">
									<label for="username" class="col-md-5 col-form-label text-md-right">Old Password :</label>
									<div class="col-md-6">
									
										<input type="password" class="form-control form-control-sm" name="password" required autofocus>
									</div>
								</div>

								<div class="form-group row">
									<label for="password" class="col-md-5 col-form-label text-md-right">New Password :</label>
									<div class="col-md-6">
										<input type="password" class="form-control form-control-sm" name="newPassword" required>
									</div>
								</div>

								<div class="form-group row">
									<label for="password" class="col-md-5 col-form-label text-md-right">New Password Verify :</label>
									<div class="col-md-6">
										<input type="password" class="form-control form-control-sm" name="verifyPassword" required>
									</div>
								</div>
								<div class="col-md-6 offset-md-4">
									<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
									<input type="submit" class="btn btn-success" name="submit" value=" Change Password ">
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
</main>


<?php require_once 'templates/footer.php'; ?>