<?php
	require_once 'config/init.php';

	if(!$pegawai->isLoggedIn()){
		Session::flash('login', 'Anda harus login');
		Redirect::to('login');
	}
	if(Session::exists('profil')){
		echo Session::flash('profil');
	}
	if(Input::get('nama')){
		$dataPegawai = $pegawai->getData(Input::get('nama'));
	}else{
		$dataPegawai = $pegawai->getData(Session::get('username'));
	}
	$tampilPegawai= $pegawai->getPegawai();
	require_once 'templates/header.php';
?>


<main class ="profil-form">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
						<div class="card">
							<div class="card-header">Halo! <?php	echo $dataPegawai['nama'];		?></div>
							<div class="card-body">
							<div class="row">
								<div class="col-md-4 offset-md-1">
								<?php echo '<img src="assets/img/'.$dataPegawai['image'].'"width="200" height="200" class="img-responsive rounded-circle""/>' ;?>
								</div>

								<div class="col-md-6">
								<table class="table table-hover text-right ">
								<tbody>
									<tr>
										<th scope="row">Nama	:</th>
										<td>
												<?php echo $dataPegawai['nama'];?>
										</td>
									</tr>

									<tr>
										<th scope="row">Kelamin	:</th>
										<td><?php 
											echo $dataPegawai['Kelamin']; ?>
										</td>
    							</tr>

									<tr>
										<th scope="row">Tanggal lahir	:</th>
										<td><?php 
											echo date('d/m/Y',strtotime($dataPegawai['tLahir'] )); ?>
										</td>
									</tr>

									<tr>
										<th scope="row">Role :</th>
										<td>
										<form  method="post" >
											<input type="radio"  id="role_0" class="radio-inline" name="role" value="0" <?php if($dataPegawai['role']==0){echo("checked");} ?>> Member 
											<input type="radio"  id="role_1" class="radio-inline" name="role" value="1" <?php if($dataPegawai['role']==1){echo("checked");} ?>> Admin 
										</td>
										</tr>

									<tr>
									<td>
									
									<?php if($pegawai->isAdmin(Session::get('username')) && $dataPegawai['role']==0 ){ ?>
										<input type="submit" class="btn btn-danger" name="submitDel" value="Delete User">
										<script>
								<?php 
									if (isset($_POST['submitDel'])){
										$pegawai->delPegawai($dataPegawai['id']);
										Redirect::to('profil');	
										Session::flash('profil', 'User telah berhasil didelete');
									 }
								?>
									</script>
									
									</td>
									<td>
											<input type="submit" class="btn btn-success" name="submitRole" value="Change Role">
											<script>
								<?php 
									if (isset($_POST['submitRole'])){
										$answer = $_POST['role'];  
										if ($answer == "0") {          
											$pegawai->updatePegawai(array(
												'role' => 0
											), $dataPegawai['id']);   
											Redirect::to('profil');
											
										}
										else if ($answer == "1"){
											$pegawai->updatePegawai(array(
												'role' => 1
											), $dataPegawai['id']);   
											
											Redirect::to('profil');
											
										}       
									 }
   										
									} ?>
									</script>
																
										</td>
										</tr>
										</form>
	
	

	


  </tbody>
</table>
	</div>

							<?php if($dataPegawai['kode']==Session::get('username')){ ?>
		
  <div class="col-md-5 offset-md-2 mt-4">
	<a class="btn btn-primary" href="change_password.php" role="button">Change Password</a>
	</div>
	<div class="col-md-5 mt-4">
	  <?php if($pegawai->isAdmin(Session::get('username'))){ ?>
		<a class="btn btn-primary" href="admin.php" role="button">View Data Pegawai</a>
	<?php } ?>
	</div>
  </div>
<?php } ?>
						</div>
				</div>
			</div>
		</div>
</main>



<?php require_once 'templates/footer.php'; ?>